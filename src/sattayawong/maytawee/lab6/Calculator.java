package sattayawong.maytawee.lab6;

import java.awt.*;

import javax.swing.*;

public class Calculator {

	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
				            public void run() {
				                createAndShowGUI();
				            }
				        });	
		}
	private static void createAndShowGUI(){
		
		JTextField texfield1 = new JTextField(33);
	
		
		JPanel panel2 = new JPanel(new GridLayout(4,4,4,4));
		//create GridLayout to add the botton
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();
		JPanel p7 = new JPanel();
		JPanel p8 = new JPanel();
		JPanel p9 = new JPanel();
		JPanel p10 = new JPanel();
		JPanel p11 = new JPanel();
		JPanel p12 = new JPanel();
		JPanel p13 = new JPanel();
		JPanel p14 = new JPanel();
		JPanel p15 = new JPanel();
		JPanel p16 = new JPanel();
		//create panel for put on botton
		
		JButton botton1 = new JButton("1");
		JButton botton2 = new JButton("2");
		JButton botton3 = new JButton("3");
		JButton botton4 = new JButton("4");
		JButton botton5 = new JButton("5");
		JButton botton6 = new JButton("6");
		JButton botton7 = new JButton("7");
		JButton botton8 = new JButton("8");
		JButton botton9 = new JButton("9");
		JButton botton10 = new JButton("0");
		JButton botton11 = new JButton("+");
		JButton botton12 = new JButton("-");
		JButton botton13 = new JButton("*");
		JButton botton14 = new JButton("/");
		JButton botton15 = new JButton("%");
		JButton botton16 = new JButton("=");
		//put detail in each botton
		p1.add(botton1);
		p2.add(botton2);
		p3.add(botton3);
		p4.add(botton4);
		p5.add(botton5);
		p6.add(botton6);
		p7.add(botton7);
		p8.add(botton8);
		p9.add(botton9);
		p10.add(botton10);
		p11.add(botton11);
		p12.add(botton12);
		p13.add(botton13);
		p14.add(botton14);
		p15.add(botton15);
		p16.add(botton16);
		//add botton in another panel
		
		panel2.add(p1);
		panel2.add(p2);
		panel2.add(p3);
		panel2.add(p4);
		panel2.add(p5);
		panel2.add(p6);
		panel2.add(p7);
		panel2.add(p8);
		panel2.add(p9);
		panel2.add(p10);
		panel2.add(p11);
		panel2.add(p12);
		panel2.add(p13);
		panel2.add(p14);
		panel2.add(p15);
		panel2.add(p16);
		
		p1.setBackground(Color.gray);
		p2.setBackground(Color.gray);
		p3.setBackground(Color.gray);
		p4.setBackground(Color.gray);
		p5.setBackground(Color.gray);
		p6.setBackground(Color.gray);
		p7.setBackground(Color.gray);
		p8.setBackground(Color.gray);
		p9.setBackground(Color.gray);
		p10.setBackground(Color.pink);
		p11.setBackground(Color.pink);
		p12.setBackground(Color.pink);
		p13.setBackground(Color.pink);
		p14.setBackground(Color.pink);
		p15.setBackground(Color.pink);
		p16.setBackground(Color.pink);
		//set color for each panel
		
		JPanel panel3 = new JPanel(new BorderLayout());
		panel3.add(texfield1 , BorderLayout.NORTH);
		panel3.add(panel2 , BorderLayout.CENTER);
		//set location of panel
		
		JFrame call = new JFrame("Simple Calculator");
		//create frame and set frame name "Simple Calculator"
		call.add(panel3);
		call.pack();
		call.setSize(500, 250);
		// to set frame to appropriate size
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (dim.width-(call.getSize().width))/2;
		int y = (dim.height-(call.getSize().height))/2;
		call.setLocation(x,y);
		// to set location of frame to center of screen
		call.setVisible(true);
		
		
	}

}
