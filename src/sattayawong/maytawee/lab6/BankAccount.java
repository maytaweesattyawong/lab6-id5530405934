package sattayawong.maytawee.lab6;

import java.awt.*;
import javax.swing.*;

public class BankAccount {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		JPanel panel1 = new JPanel(new FlowLayout());
		// create 3 botton <Withdraw> <Deposit> <Exit>
		JButton withdrawbotton = new JButton("Withdraw");
		JButton depositbotton = new JButton("Deposit");
		JButton exitbotton = new JButton("Exit");
		panel1.add(withdrawbotton);
		panel1.add(depositbotton);
		panel1.add(exitbotton);
		// to add botton into panel

		JPanel panel2 = new JPanel(new GridLayout(3, 2));
		JLabel previouslabel = new JLabel("Previous Balane: ");
		JLabel amountlabel = new JLabel("Amount: ");
		JLabel currentlabel = new JLabel("Current Balane: ");
		JTextField texfield1 = new JTextField("1000");
		JTextField texfield2 = new JTextField();
		JTextField texfield3 = new JTextField();

		panel2.add(previouslabel);
		panel2.add(texfield1);
		panel2.add(amountlabel);
		panel2.add(texfield2);
		panel2.add(currentlabel);
		panel2.add(texfield3);

		JPanel panel3 = new JPanel(new BorderLayout());
		panel3.add(panel2, BorderLayout.NORTH);
		panel3.add(panel1, BorderLayout.SOUTH);

		JFrame call = new JFrame("Simple Bank Account");
		//create frame and set frame name "Simple Bank Account"
		call.add(panel3);
		call.pack();
		call.setSize(400, 150);
		// to set frame to appropriate size
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (dim.width - (call.getSize().width)) / 2;
		int y = (dim.height - (call.getSize().height)) / 2;
		call.setLocation(x, y);
		// to set location of frame to center of screen
		call.setVisible(true);

	}

}
